#ifndef ASTEROID_H
#define ASTEROID_H

#include <SFML/Graphics.hpp>
#include "Actor.hpp"

class Asteroid : public Actor
{
public:
	Asteroid(sf::RenderWindow *window) : Actor(window), radius(0) {};
	void draw();
	void update(sf::Time dt);
	float radius;
private:
};

#endif
