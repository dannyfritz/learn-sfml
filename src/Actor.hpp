#ifndef ACTOR_H
#define ACTOR_H

#include <SFML/Graphics.hpp>

class Actor
{
public:
	Actor(sf::RenderWindow *window) : x(0), y(0), window(window) {};
	virtual void draw();
	virtual void update(sf::Time dt);
	float x, y;
protected:
	sf::RenderWindow *window;
};

#endif
