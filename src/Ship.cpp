#include "Ship.hpp"

void Ship::update(sf::Time dt) {
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) {
		x -= 100 * dt.asSeconds();
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) {
		x += 100 * dt.asSeconds();
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) {
		y -= 100 * dt.asSeconds();
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) {
		y += 100 * dt.asSeconds();
	}
}

void Ship::draw() {
	sf::RectangleShape rectangle(sf::Vector2f(width, height));
	rectangle.setPosition(x, y);
	window->draw(rectangle);
}