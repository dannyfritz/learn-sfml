#include "Game.hpp"

Game::Game () {
	sf::RenderWindow window;
}

void Game::run() {
	window.create(sf::VideoMode(800, 600), "Shoot!");
	window.setVerticalSyncEnabled(true);
	actors.push_back(new Ship(&window));
	while (window.isOpen())
	{
		sf::Time dt = clock.restart();

		update(dt);
		draw();
	}
}

void Game::events() {
	sf::Event event;
	while (window.pollEvent(event))
	{
		//std::cout << event.type << std::endl;
		if (event.type == sf::Event::Closed) {
			window.close();
		}
	}
}

void Game::update(sf::Time dt) {
	events();
	std::list<Actor*>::iterator actor;
	for (actor = actors.begin(); actor != actors.end(); actor++) {
		(*actor)->update(dt);
	}
}

void Game::draw() {
	window.clear();
	std::list<Actor*>::iterator actor;
	for (actor = actors.begin(); actor != actors.end(); actor++) {
		(*actor)->draw();
	}
	window.display();
}
