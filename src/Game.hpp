#include <iostream>
#include <list>
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include "Ship.hpp"
#include "Asteroid.hpp"

class Game
{
public:
	Game();
	void run();
protected:
	sf::RenderWindow window;
	sf::Clock clock;
	std::list<Actor*> actors;
	void events();
	void update(sf::Time dt);
	void draw();
};
