#ifndef ASTEROID_H
#define ASTEROID_H

#include <SFML/Graphics.hpp>
#include "Actor.hpp"

class Ship: public Actor
{
public:
	Ship(sf::RenderWindow *window) : Actor(window), width(20), height(20) {};
	void draw();
	void update(sf::Time dt);
	float width, height;
private:
};

#endif
