CC=g++
CFLAGS=-Wall
SFMLDEPS=-lsfml-graphics -lsfml-window -lsfml-system
CLASSES=src/Game.cpp src/Actor.cpp src/Ship.cpp src/Asteroid.cpp

all: game

game:
	mkdir -p bin
	clear && clear
	$(CC) $(CFLAGS) src/main.cpp $(CLASSES) -o bin/game $(SFMLDEPS)

clean:
	rm -rf bin

run: game
	./bin/game
